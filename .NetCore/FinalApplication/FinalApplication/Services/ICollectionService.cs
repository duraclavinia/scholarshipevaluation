﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalApplication.Services
{
    public interface ICollectionService<T>
    {
        Task<List<T>> GetAllInterns();

        Task<T> GetInternById(Guid id);

        Task<bool> Create(T model);

        Task<bool> Update(Guid id, T model);

        Task<bool> Delete(Guid id);

    }
}
