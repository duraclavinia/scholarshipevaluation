﻿using FinalApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalApplication.Services
{
    public interface IInternCollectionService : ICollectionService<Intern>
    {
    }
}
